package pjatk.inf.s16787.finalproject;

import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.context.ConfigurableApplicationContext;
import pjatk.inf.s16787.finalproject.gui.controller.LogInController;

import javax.swing.*;

@SpringBootApplication
public class FinalProjectApplication {
    public static void main(String[] args) {
        ConfigurableApplicationContext ctx = new SpringApplicationBuilder(FinalProjectApplication.class)
                .headless(false).run(args);

        SwingUtilities.invokeLater( () -> {
            LogInController controller = ctx.getBean(LogInController.class);
            controller.showView();
        });
    }
}
