package pjatk.inf.s16787.finalproject.gui.view;

import lombok.Data;
import org.springframework.stereotype.Component;

import javax.swing.*;

@Component
@Data
public class LogInView extends JDialog {
  private JButton logInButton;
  private JTextField usernameField;
  private JPasswordField passwordField;
  private JPanel mainPanel;

  public LogInView() {
    setTitle("Log in");
    setSize(400, 300);
    setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
    add(mainPanel);
  }
}
