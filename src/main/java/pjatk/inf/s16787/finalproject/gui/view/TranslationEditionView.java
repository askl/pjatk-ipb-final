package pjatk.inf.s16787.finalproject.gui.view;

import lombok.Data;
import org.springframework.stereotype.Component;

import javax.swing.*;
import java.util.Arrays;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

@Component
@Data
public class TranslationEditionView extends JFrame {
    private JPanel mainPanel;
    private JTextArea originalTextArea;
    private JTextArea translationTextArea;
    private JButton saveButton;
    private JList pageList;
    private JList sectionsList;

    public TranslationEditionView() {
        setTitle("Edit translation");
        setSize(600, 400);
        setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        add(mainPanel);

        pageList.setModel(new DefaultListModel<Integer>());
        DefaultListModel<Integer> pageModel = (DefaultListModel<Integer>) pageList.getModel();
        pageModel.addAll(IntStream.range(1, 10).boxed().collect(Collectors.toList()));

        sectionsList.setModel(new DefaultListModel<Integer>());
        DefaultListModel<Integer> sectionModel = (DefaultListModel<Integer>) sectionsList.getModel();
        sectionModel.addAll(IntStream.range(1, 10).boxed().collect(Collectors.toList()));
    }
}
