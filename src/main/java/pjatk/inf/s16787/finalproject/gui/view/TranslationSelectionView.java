package pjatk.inf.s16787.finalproject.gui.view;

import lombok.Data;
import org.springframework.stereotype.Component;

import javax.swing.*;
import java.util.Arrays;

@Component
@Data
public class TranslationSelectionView extends JFrame {
  private JPanel mainPanel;
  private JButton openButton;
  private JList titleList;
  private JList languageList;
  private JMenuItem logOutMenuItem;
  private JMenuItem closeMenuItem;

  public TranslationSelectionView() {
    setTitle("Select translation");
    setSize(800, 600);
    setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    addMenuBar();

    String data1[] = {"Hamlet", "Ghost world"};
    titleList.setModel(new DefaultListModel<String>());
    DefaultListModel<String> titleModel = (DefaultListModel<String>) titleList.getModel();
    titleModel.addAll(Arrays.asList(data1));

    String data2[] = {"English", "Polish"};
    languageList.setModel(new DefaultListModel<String>());
    DefaultListModel<String> languageModel = (DefaultListModel<String>) languageList.getModel();
    languageModel.addAll(Arrays.asList(data2));

    add(mainPanel);
  }

  private void addMenuBar() {
    JMenuBar bar = new JMenuBar();
    JMenu menu = new JMenu();
    menu.setText("Menu");

    logOutMenuItem = new JMenuItem();
    logOutMenuItem.setText("Log out");
    menu.add(logOutMenuItem);

    closeMenuItem = new JMenuItem();
    closeMenuItem.setText("Exit");
    menu.add(closeMenuItem);

    bar.add(menu);
    this.setJMenuBar(bar);
  }
}
