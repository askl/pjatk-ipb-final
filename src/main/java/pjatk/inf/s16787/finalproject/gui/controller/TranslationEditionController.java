package pjatk.inf.s16787.finalproject.gui.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import pjatk.inf.s16787.finalproject.gui.view.TranslationEditionView;

import javax.annotation.PostConstruct;
import javax.swing.*;

@Controller
@RequiredArgsConstructor
public class TranslationEditionController {
  private final TranslationEditionView view;

  @PostConstruct
  private void initListeners() {
    view.getSaveButton().addActionListener(a -> {
      JOptionPane.showConfirmDialog(view, "Changes saved.", "Success", JOptionPane.DEFAULT_OPTION);
    });
  }

  public void showView() {
    view.setVisible(true);
  }
}