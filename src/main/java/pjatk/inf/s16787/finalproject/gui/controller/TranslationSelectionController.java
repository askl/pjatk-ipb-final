package pjatk.inf.s16787.finalproject.gui.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import pjatk.inf.s16787.finalproject.gui.view.TranslationSelectionView;

import javax.annotation.PostConstruct;
import javax.swing.*;
import java.util.List;

@Controller
@RequiredArgsConstructor
public class TranslationSelectionController {
  private final TranslationSelectionView view;
  private final TranslationEditionController editController;

  @PostConstruct
  private void initListeners() {
    view.getCloseMenuItem().addActionListener(a -> {
      view.dispose();
    });

    view.getOpenButton().addActionListener(a -> {
      editController.showView();
    });
  }

  public void showView() {
    view.setVisible(true);
  }
}
