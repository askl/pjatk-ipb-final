package pjatk.inf.s16787.finalproject.gui.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import pjatk.inf.s16787.finalproject.gui.view.LogInView;

import javax.annotation.PostConstruct;

@Controller
@RequiredArgsConstructor
public class LogInController {
  private final LogInView view;

  private final TranslationSelectionController selectController;

  public void showView() {
    view.setVisible(true);
  }

  @PostConstruct
  private void initListeners() {
    view.getLogInButton().addActionListener(a -> {
      selectController.showView();
    });
  }
}