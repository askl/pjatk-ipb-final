# Final project description
The process begins when an author of a comic book sends the first draft of the comic. The draft arrives to a manager which will review it, and if he is interested in publishing it, he will prepare a contract and send it to an author. If the author signs it, the draft will be sent to the translator to prepare translations. The translations are then reviewed by the manager. The process repeats until the translations are accepted. Then the draft is forwarded to an editor, which will prepare the comic for printing. The manager will also have to review and accept it. After the draft is ready it will be sent to the printing house, and when the print arrives it will be sent to a warehouse and the process ends.

Buisness use case diagram:
![Buisness use case diagram](diagrams/Buisness%20use%20case%20diagram.jpg)

Buisness Analisys diagram:
![Buisness Analisys diagram](diagrams/Buisness%20sequence%20diagram.jpg)

BPMN diagram with the current form of this business process:
![Buisness use case diagram](diagrams/final.png)

BPMN diagram showing the new form of the business proces:
![Buisness use case diagram](diagrams/final_new.png)

Use case diagram:
![Use case diagram](diagrams/Use%20Case%20Diagram.jpg)

Class diagram:
![Class diagram](diagrams/Class%20Diagram.jpg)
